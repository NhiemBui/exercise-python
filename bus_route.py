# -*- coding: utf-8 -*-
"""
"""

n = int(input())
a = sorted(map(int, input().split()))
distance_list = [a[i] - a[i - 1] for i in range(1, n)]
min_distance = min(distance_list)
print(min_distance, distance_list.count(min_distance))
