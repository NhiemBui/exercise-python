# -*- coding: utf-8 -*-
"""
"""

import model as m

if __name__ == "__main__":
    current_position = (4, 4)
    list_positions = [(3, 3), (5, 1), (4, 2), (0, 6)]
    p = m.Queen(current_position, list_positions)
    print(p.check_list_positions())
