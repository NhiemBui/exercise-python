# -*- coding: utf-8 -*-
"""
"""
from itertools import product


class Piece(object):
    """Piece Superclass. All piece objects inherit the Piece class"""

    def __init__(self, current_position, list_positions):
        self.current_position = current_position
        self.list_positions = list_positions
        self.x = None
        self.y = None
        self.next_x = None
        self.next_y = None

    def set_position(self, next_position):
        self.x = self.current_position[0]
        self.y = self.current_position[1]
        self.next_x = next_position[0]
        self.next_y = next_position[1]

    def validate_position(self):
        if self.next_x < 0 or self.next_y < 0 or self.next_x >= 8 or self.next_y >= 8:
            return False

        if self.x < 0 or self.y < 0 or self.x >= 8 or self.y >= 8:
            return False

        return True

    def check_next_position(self):
        pass

    def check_list_positions(self):
        """"""
        is_valid = True
        for position in self.list_positions:
            self.set_position(position)
            if self.validate_position() and self.check_next_position():
                self.current_position = position
            else:
                is_valid = False
                break
        return is_valid


class Pawn(Piece):
    def check_next_position(self):
        """Move pawn 1 space forward, unless on the first move of *this* pawn, then 2 spaces is allowed"""
        if self.next_x == self.x:
            if self.next_y == 1 and (self.next_y - self.y) <= 2:
                return True
            elif self.next_y == 6 and (self.y - self.next_y) <= 2:
                pass
            elif abs(self.next_y - self.y) == 1:
                return True
        return False


class Rook(Piece):
    def check_next_position(self):
        """Move Rook * spaces on vertical or horizontal path"""
        if self.next_x == self.x or self.next_y == self.y:
            return True
        return False


class Knight(Piece):
    def check_next_position(self):
        """Move Knight in L shape in any direction"""
        positions = list(product([self.x - 1, self.x + 1], [self.y - 2, self.y + 2])) + \
                    list(product([self.x - 2, self.x + 2], [self.y - 1, self.y + 1]))
        if (self.next_x, self.next_y) in positions:
            return True
        else:
            return False


class Bishop(Piece):
    def check_next_position(self):
        """Move bishop * spaces on diagonal path"""
        if abs(self.next_x - self.x) == abs(self.next_y - self.y):
            return True
        return False


class Queen(Piece):
    def check_next_position(self):
        """Move Queen * spaces in any direction"""
        if self.next_x == self.x or self.next_y == self.y or abs(self.next_x - self.x) == abs(self.next_y - self.y):
            return True
        return False


class King(Piece):
    def check_next_position(self):
        """Move King 1 space in any direction"""
        if abs(self.next_x - self.x) <= 1 and abs(self.next_y - self.y) <= 1:
            return True
        return False
