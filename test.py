# -*- coding: utf-8 -*-
"""
"""

import unittest

import model as m


class PieceTestCase(unittest.TestCase):
    def setUp(self):
        self.current_position = (4, 4)


class PawnTestCase(PieceTestCase):
    def test_valid_step(self):
        self.list_positions = [(4, 3), (4, 2), (4, 1), (4, 0)]
        p = m.Pawn(self.current_position, self.list_positions)
        self.assertTrue(p.check_list_positions())

    def test_invalid_step(self):
        self.list_positions = [(4, 3), (5, 3), (4, 1), (4, 0)]
        p = m.Pawn(self.current_position, self.list_positions)
        self.assertFalse(p.check_list_positions())


class RookTestCase(PieceTestCase):
    def test_valid_step(self):
        self.list_positions = [(7, 4), (7, 1), (4, 1), (4, 6)]
        p = m.Rook(self.current_position, self.list_positions)
        self.assertTrue(p.check_list_positions())

    def test_invalid_step(self):
        self.list_positions = [(7, 4), (7, 1), (2, 2), (4, 6)]
        p = m.Rook(self.current_position, self.list_positions)
        self.assertFalse(p.check_list_positions())


class KnightTestCase(PieceTestCase):
    def test_valid_step(self):
        self.list_positions = [(2, 5), (1, 7), (3, 6), (1, 5), (2, 7), (3, 5), (2, 3), (4, 4)]
        p = m.Knight(self.current_position, self.list_positions)
        self.assertTrue(p.check_list_positions())

    def test_invalid_step(self):
        self.list_positions = [(2, 5), (1, 7), (3, 6), (0, 5), (2, 7), (3, 5), (2, 3), (4, 4)]
        p = m.Knight(self.current_position, self.list_positions)
        self.assertFalse(p.check_list_positions())


class BishopTestCase(PieceTestCase):
    def test_valid_step(self):
        self.list_positions = [(3, 3), (5, 1), (4, 2), (0, 6)]
        p = m.Bishop(self.current_position, self.list_positions)
        self.assertTrue(p.check_list_positions())

    def test_invalid_step(self):
        self.list_positions = [(3, 3), (5, 1), (4, 4), (0, 6)]
        p = m.Bishop(self.current_position, self.list_positions)
        self.assertFalse(p.check_list_positions())


class QueenTestCase(PieceTestCase):
    def test_valid_step(self):
        self.list_positions = [(2, 6), (2, 1), (6, 1), (6, 7), (7, 6), (7, 1), (2, 1), (4, 3)]
        p = m.Queen(self.current_position, self.list_positions)
        self.assertTrue(p.check_list_positions())

    def test_invalid_step(self):
        self.list_positions = [(2, 6), (2, 1), (6, 1), (0, 7), (7, 6), (7, 1), (2, 1), (4, 3)]
        p = m.Queen(self.current_position, self.list_positions)
        self.assertFalse(p.check_list_positions())


class KingTestCase(PieceTestCase):
    def test_valid_step(self):
        self.list_positions = [(3, 5), (2, 4), (3, 4), (4, 3)]
        p = m.King(self.current_position, self.list_positions)
        self.assertTrue(p.check_list_positions())

    def test_invalid_step(self):
        self.list_positions = [(4, 3), (5, 2), (4, 2), (2, 1)]
        p = m.King(self.current_position, self.list_positions)
        self.assertFalse(p.check_list_positions())


def bus_route(n, a):
    a = sorted(a)
    distance_list = [a[i] - a[i - 1] for i in range(1, n)]
    min_distance = min(distance_list)
    return [min_distance, distance_list.count(min_distance)]


class BusRouteTestCase(unittest.TestCase):
    def test_input_3(self):
        n = 3
        a = [7, 10, 12]
        self.assertListEqual(bus_route(n, a), [2, 1])

    def test_input_4(self):
        n = 4
        a = [6, -3, 0, 4]
        self.assertListEqual(bus_route(n, a), [2, 1])

    def test_input_5(self):
        n = 5
        a = [-7, -5, -4, -3, -1]
        self.assertListEqual(bus_route(n, a), [1, 2])

    def test_input_20(self):
        n = 20
        a = [553280626, 553280623, 553280627, 553280624, 553280625, 553280618, 553280620, 553280629, 553280637,
             553280631, 553280628, 553280636, 553280635, 553280632, 553280634, 553280622, 553280633, 553280621,
             553280630, 553280619]
        self.assertListEqual(bus_route(n, a), [1, 19])


if __name__ == '__main__':
    unittest.main()
